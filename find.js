function find(elements = [],cb = undefined) {
    if (elements.length === 0 || cb === undefined){
        return ([])
    }
    else{
        for (let index = 0; index < elements.length; index++) {
            result = cb(elements[index])
    
            if (result === true){
                return elements[index]
            }
        }

    } 
    
}


module.exports = find;
