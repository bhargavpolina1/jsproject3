function flatten(elements = []) {
if (elements.length === 0){
    return ([])
}

    let flattenedArray = []
    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index])) {
            flattenedArray = flattenedArray.concat(flatten(elements[index]))
            
        } else {
            flattenedArray.push(elements[index]);
            
        }
    
    }

    return flattenedArray

}

module.exports = flatten;