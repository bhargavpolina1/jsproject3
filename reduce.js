function reduce(elements = [], cb = undefined, startingValue = undefined) {
    if (elements.length === 0 || cb === undefined || startingValue === undefined){
        return ([])
    }
    else{
        let reduced_value = startingValue
        for (let index = 0; index < elements.length; index++) {
            reduced_value = cb(reduced_value, elements[index])
        }
        return reduced_value
    }   
   
}

module.exports = reduce;
