function map(elements = [], cb = undefined){
    if (elements.length === 0 || cb === undefined){
        return []
    }
    else{
        let eachArray = []
        for (let index = 0; index < elements.length;index++){
        result = cb(elements[index])
        eachArray.push(result)
    }
    return eachArray
    }  
}

module.exports = map



