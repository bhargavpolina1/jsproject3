function filter(elements = [],cb = undefined) {

    let filteredArray= []
    if (elements.length === 0 || cb === undefined){
        return ([])
    } else{
        for (let index = 0; index < elements.length; index++) {
            result = cb(elements[index])
    
            if (result === true){
                filteredArray.push(elements[index])
            }
        }

        return filteredArray

    }
    
    
}


module.exports = filter;
